// ---------------NOTE----------------
// Pinasok ko lang po sila lahat sa function para magawa
// ko silang asyn para sunod ang pag labas sa console
// And pa drop down ng napakalaking array.
// Meron pa po siyang ipapakita. Hehe.

const thisFunction = async () => {
  // For Number 3 to 4
  const titleArray = [];
  await fetch("https://jsonplaceholder.typicode.com/todos", {
    method: "GET",
    headers: { "Content-Type": "application/json" },
  })
    .then((res) => res.json())
    .then((converted) =>
      converted.map((item) => {
        titleArray.push(item.title);
      })
    );
  // ------ Can Also use code below -----------
  //   .then((converted) => converted.map(({ title }) => ({ title })))
  //   .then((resultOfMap) => console.log(resultOfMap));
  console.log(titleArray);

  // For Number Five to Six
  await fetch("https://jsonplaceholder.typicode.com/todos/1", {
    method: "GET",
    headers: { "Content-Type": "application/json" },
  })
    .then((res) => res.json())
    .then((converted) =>
      console.log(`Title: ${converted.title}
Completed: ${converted.completed}`)
    );

  // For Number 7
  await fetch("https://jsonplaceholder.typicode.com/todos/", {
    method: "POST",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify({
      title: "This is a New Title",
      completed: "false",
      userId: 200,
    }),
  })
    .then((res) => res.json())
    .then((converted) => console.log(converted));

  // For Number 8
  await fetch("https://jsonplaceholder.typicode.com/todos/1", {
    method: "PUT",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify({
      title: "This is an Updated Title",
      completed: "true",
      userId: 200,
    }),
  })
    .then((res) => res.json())
    .then((converted) => console.log(converted));

  // For Number 9
  await fetch("https://jsonplaceholder.typicode.com/todos/5", {
    method: "PUT",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify({
      title: "Updated Title Again",
      description: "Some Description",
      status: "Completed",
      date_completed: "Today",
      userId: 100,
    }),
  })
    .then((res) => res.json())
    .then((converted) => console.log(converted));

  // For Number 10
  await fetch("https://jsonplaceholder.typicode.com/todos/5", {
    method: "PATCH",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify({
      description: "Updated Description",
    }),
  })
    .then((res) => res.json())
    .then((converted) => console.log(converted));

  // For Number 11
  await fetch("https://jsonplaceholder.typicode.com/todos/5", {
    method: "PATCH",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify({
      status: "Completed",
      date_completed: "Just Now",
    }),
  })
    .then((res) => res.json())
    .then((converted) => console.log(converted));

  // For Number 12
  await fetch("https://jsonplaceholder.typicode.com/todos/20", {
    method: "DELETE",
  });
};

thisFunction();
